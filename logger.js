
const EventEmitter = require('events');

var url = 'http://my-logger';


class Logger extends EventEmitter {
    // Register and event
    log(message) {
        console.log(message);
        this.emit('Message emitted', {id: 1, url, message});
    }
}


module.exports = Logger;