const Logger = require('./logger');
const http = require('http');

var _ = require('underscore');
var logger = new Logger();

const server = http.createServer((req, res) => {
    if (req.url === '/'){
        res.write('Home ...');
    }

    if(req.url === '/api'){
        res.write(JSON.stringify([1,2,3]));
    }
    res.end();
});

let response = _.contains([1,2,3],2);
console.log(response);

server.on('connection', (socket) => {
    console.log('Connection established !');
})

server.listen(3000);
console.log('Listening on port 3000 ...')


logger.on('Message loaded', arg => {
    console.log('Listener called', arg);
});

logger.log('hola que tal');